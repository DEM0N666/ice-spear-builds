#  Ice-Spear - Builds
![alt Ice-Spear-Builds](logo.png)

Prebuild packages for Ice-Spear.

Download the .7z file and extract it.

Inside the new folder run "ice-spear-editor.exe" (windows) or "ice-spear-editor" (linux).

### Wiki
If you want to know how to use this tool, please check out the Wiki: <br/>
https://gitlab.com/ice-spear-tools/ice-spear/wikis/home

<br />
### License
___
Licensed under GNU GPLv3.  
For more information see the LICENSE file in the project's root directory